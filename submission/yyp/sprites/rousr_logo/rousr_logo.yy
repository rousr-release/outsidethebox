{
    "id": "2ba056db-16cd-47e8-89e4-8c45e858e1aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rousr_logo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 92,
    "bbox_left": 51,
    "bbox_right": 99,
    "bbox_top": 58,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5834dde4-eff7-4734-adfb-a7c9686cc4c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ba056db-16cd-47e8-89e4-8c45e858e1aa",
            "compositeImage": {
                "id": "fe3867ce-b1fc-4743-a8c4-53bc0a0b5d95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5834dde4-eff7-4734-adfb-a7c9686cc4c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca27b44b-5f54-43c4-a518-76a615f2a480",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5834dde4-eff7-4734-adfb-a7c9686cc4c9",
                    "LayerId": "a6dfb3d6-b3fb-438c-a72e-2fb2a4fa4712"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "a6dfb3d6-b3fb-438c-a72e-2fb2a4fa4712",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ba056db-16cd-47e8-89e4-8c45e858e1aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 36,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 0,
    "yorig": 0
}