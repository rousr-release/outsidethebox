{
    "id": "3e2c6ad6-35f5-4437-ac55-13a60009c3a1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "imgui",
    "eventList": [
        {
            "id": "2bc0bdf4-b81a-4fdb-a05a-19c963eb00c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "3e2c6ad6-35f5-4437-ac55-13a60009c3a1"
        },
        {
            "id": "942f6bfc-e702-4003-aeb8-c0260490fc1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3e2c6ad6-35f5-4437-ac55-13a60009c3a1"
        },
        {
            "id": "f9820192-d89f-4522-a16b-1d5771d8bdeb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "3e2c6ad6-35f5-4437-ac55-13a60009c3a1"
        },
        {
            "id": "92a8241e-e556-401a-960e-7a9948beebac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3e2c6ad6-35f5-4437-ac55-13a60009c3a1"
        },
        {
            "id": "02fcb83b-94b5-4d25-b54e-19a9d9b8be8f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "3e2c6ad6-35f5-4437-ac55-13a60009c3a1"
        },
        {
            "id": "f0c568b0-9069-4879-bff5-26c51aa759e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "3e2c6ad6-35f5-4437-ac55-13a60009c3a1"
        },
        {
            "id": "67f0e56f-7182-46d1-a456-de61e0dc0adb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "3e2c6ad6-35f5-4437-ac55-13a60009c3a1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}