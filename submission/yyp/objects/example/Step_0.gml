///@desc example - step
if (keyboard_check_pressed(vk_escape))
	game_end();

if (imguigml_ready()) {

	var size       = [ 0, 0 ];
  var pos        = [ 0, 0 ];
	var cursor_pos = [ 0, 0 ];
  
	var browser_pos = [ 0, 0 ];
	
	var close_file = false;
	var load_file  = false;
	
	imguigml_set_next_window_pos(50, 50, EImGui_Cond.Once);
	var ret = imguigml_begin("outsideTheBox v2.0.0", undefined ,EImGui_WindowFlags.NoResize | EImGui_WindowFlags.NoMove | EImGui_WindowFlags.NoCollapse); 
	if (ret[0]) {
			if (imguigml_button("Show Memory Usage")) {
				if (!Show_mem_usage)
					meminfo_start();
				Show_mem_usage = true;
			}
			imguigml_same_line();
			if (imguigml_button("Sandbox test.txt")) {
				var entry = array_create(EOTBFileSystemEntry.Num);
				entry[@ EOTBFileSystemEntry.Name] = "test.txt";
				entry[@ EOTBFileSystemEntry.AbsolutePath] = working_directory + "test.txt";
				Working_file = entry;
				load_file = true;
			}
			imguigml_same_line();
			ret = imguigml_checkbox("Borderless", Is_borderless);
			if (ret[0]) {
				Is_borderless = ret[1];
				if (Is_borderless) 
					otb_set_borderless();
				else
					otb_set_windowed();
			}
			
			imguigml_push_item_width(max(imguigml_get_content_region_avail_width(), 400));
			ret = imguigml_input_text("##otbFolder", Working_path, 256);
			if (ret[0]) Working_path = ret[1];
			List_dirs |= ret[0];
			
			if (imguigml_button("Browse for Folder")) {
				var path = otb_browse_for_folder(Working_path);	
				if (path != undefined && Working_path != path) {
					Working_path = path;
					List_dirs |= true;
				}
			}
			imguigml_same_line();			
			if (imguigml_button("Browse for Folder (Asyncronously)")) {
				otb_browse_for_folder_async(Working_path);	
			}

			if (List_dirs) {
				var working_entry = otb_filesystem_path(Working_path);
				Working_files_num  = undefined;
				if (working_entry != undefined && !working_entry[EOTBFileSystemEntry.IsOther]) {
					Working_file_list = otb_filesystem_list_directory(Working_path);	
					if (Working_file_list != undefined) {
						Working_files_num  = array_length_1d(Working_file_list);
						Working_file_names = array_create(Working_files_num);
						Working_file_index = 0;
					
						var filtered_files = [ ];
						var num_files = 0;
						for (var i = 0; i < Working_files_num; ++i) {
							var entry = Working_file_list[i];
							if (entry[EOTBFileSystemEntry.IsOther])
								continue;
						
							filtered_files[@ num_files] = entry;
							Working_file_names[@ num_files++] = entry[EOTBFileSystemEntry.Name];	
						}
					
						Working_file_list = filtered_files;
						Working_files_num = num_files;
					}
				}
				List_dirs = false;
			}
			
			if (Working_files_num != undefined) {
				imguigml_separator();
				imguigml_text("Folder Contents:");
				ret = imguigml_list_box("##otbFolderContentsLabel", Working_file_index, Working_file_names);
				if (ret[0]) Working_file_index = ret[1];
				
				var entry = Working_file_list[Working_file_index];
				var is_directory = entry[EOTBFileSystemEntry.IsDirectory];
				var is_file = !is_directory && (entry[EOTBFileSystemEntry.IsRegularFile] || entry[EOTBFileSystemEntry.IsSymLink]);
				var size = entry[EOTBFileSystemEntry.Size];
				
				var file_can_open = is_file;
				var pushed = false;

				if (is_directory && imguigml_button("Go")) {
						Working_path = entry[EOTBFileSystemEntry.AbsolutePath];
						List_dirs = true;
				} else if (is_file) {
					
					if (!file_can_open) {
						imguigml_push_style_var(EImGui_StyleVar.Alpha, 0.6);
						pushed = true;
					}
					
					if (imguigml_button("Open File") && file_can_open) {
						// open the file in the text editor
						Working_file = entry;
						load_file = true;
					}
				}
				
				if (pushed)
					imguigml_pop_style_var();
			}

			cursor_pos = imguigml_get_cursor_screen_pos();
			pos        = imguigml_get_window_pos(); 
			browser_pos = pos;
			imguigml_set_window_size("outsideTheBox v2.0.0", max(400, imguigml_get_content_region_avail_width()), cursor_pos[1] - pos[1], EImGui_Cond.Always);
			size       = imguigml_get_window_size();
			imguigml_pop_item_width();
			
	} 
	imguigml_end();

	if (Working_files_num != undefined && Working_file_index != undefined && Working_file_index < Working_files_num) {
		var entry = Working_file_list[Working_file_index];
		var is_directory = entry[@ EOTBFileSystemEntry.IsDirectory];
		var label = is_directory ? "Directory Details" : "File Details";
	
		imguigml_set_next_window_pos(pos[0], pos[1] + size[1], EImGui_Cond.Always);
		imguigml_push_style_var(EImGui_StyleVar.WindowRounding, false);
		ret = imguigml_begin(label + "##otbFileDetail", undefined, EImGui_WindowFlags.NoResize | EImGui_WindowFlags.NoMove | EImGui_WindowFlags.NoCollapse);
		if (ret[0]) {
			imguigml_text("Name: " + entry[EOTBFileSystemEntry.Name]);
			imguigml_text("Size: " + string(entry[EOTBFileSystemEntry.Size]));
			imguigml_text(is_directory ? "Directory" : "File");
			cursor_pos = imguigml_get_cursor_screen_pos();
			pos        = imguigml_get_window_pos();
			var old_size = size;
			size       = imguigml_get_window_size();
			size[0] = old_size[0];
			imguigml_set_window_size(label + "##otbFileDetail", old_size[0], cursor_pos[1] - pos[1], EImGui_Cond.Always);
		}
		imguigml_end();
	}

	if (Working_file != undefined) {
		var file_name = Working_file[EOTBFileSystemEntry.Name];
    imguigml_set_next_window_pos(browser_pos[0] + size[0], browser_pos[1], EImGui_Cond.Always);
    imguigml_set_next_window_size(650, 550, EImGui_Cond.Appearing);
		imguigml_push_style_var(EImGui_StyleVar.WindowRounding, false);
		
		var ret = imguigml_begin("Text Editor - " + file_name + "##otbText", true, EImGui_WindowFlags.NoMove | EImGui_WindowFlags.NoCollapse);
    if (ret[0] && ret[1]) {
      imguigml_texteditor_render(Text_editor, "##otbEditor");
		} else if (!ret[1]) 
			close_file = true;
		
		pos  = imguigml_get_window_pos();
		size = imguigml_get_window_size();
    imguigml_end();
		imguigml_pop_style_var();
		
		imguigml_set_next_window_pos(pos[0], pos[1] + size[1], EImGui_Cond.Always);
		imguigml_set_next_window_size(size[0], 18, EImGui_Cond.Always);
    imguigml_begin("##otbEditorButtons", undefined, EImGui_WindowFlags.NoTitleBar | EImGui_WindowFlags.NoResize | EImGui_WindowFlags.NoMove | EImGui_WindowFlags.NoCollapse); 
		
		if (imguigml_button("Save")) {
			var fh = otb_file_open(Working_file[EOTBFileSystemEntry.AbsolutePath], oTB.Write);
			if (fh >= 0) {
				var text = imguigml_texteditor_get_text(Text_editor);
				file_text_write_string(fh, imguigml_texteditor_get_text(Text_editor));
				otb_file_close(fh);
			}
		} 
		imguigml_same_line();
		if (imguigml_button("Reload")) {
			load_file = true;
		}
		imguigml_same_line();
		if (imguigml_button("Exit")) {
			close_file = true;
		}
		
		imguigml_end();
		
  } else if (Text_editor != undefined) {
		Text_editor = imguigml_texteditor_destroy(Text_editor);
	}
}

if (load_file) {
	Working_file_contents = "";
	var fh = otb_file_open(Working_file[EOTBFileSystemEntry.AbsolutePath], oTB.Read);
	if (fh >= 0) {
		while (!file_text_eof(fh)) Working_file_contents += file_text_readln(fh);
		otb_file_close(fh);
	}
						
	if (Text_editor == undefined)
		Text_editor = imguigml_texteditor_create();
	imguigml_texteditor_set_text(Text_editor, Working_file_contents);
}

if (close_file) {
	Working_file = undefined;	
	imguigml_texteditor_destroy(Text_editor);
	Text_editor = undefined;
}

if (Show_mem_usage) {
	Mem_usage_next_sample -= delta_time;
	if (Mem_usage_next_sample <= 0) {
		Mem_usage_next_sample = Mem_usage_sample_time * 1000000;
		
		var usage = Mem_usage_history[0];
		var prv   = Mem_usage_history[1];
		var ws    = Mem_usage_history[2];
		
		usage[@ Mem_usage_sample_index] = meminfo_get_mem_usage();
    prv[@ Mem_usage_sample_index]   =	meminfo_get_private_usage();
		ws[@ Mem_usage_sample_index]    =	meminfo_get_working_set_usage();
		
		Mem_usage_sample_index++;
		Mem_usage_sample_index %= Mem_usage_count;
	}
	var ret =	imguigml_begin("Memory Usage", true);
	if (!ret[1]) {
		meminfo_stop();
		Show_mem_usage = false;
	}
	else if (ret[0]) {
		imguigml_text("Memory Usage:             "); imguigml_same_line(); imguigml_text(string(meminfo_get_mem_usage())); 
		imguigml_plot_lines("##MemUsage",  Mem_usage_history[0], 0, "");
		
		imguigml_text("Virtual Memory:           "); imguigml_same_line(); imguigml_text(string(meminfo_get_private_usage())); 
		imguigml_plot_lines("##MemUsage2", Mem_usage_history[1], 0, "");
		
		imguigml_text("Physical (Shared) Memory: "); imguigml_same_line(); imguigml_text(string(meminfo_get_working_set_usage())); 
		imguigml_plot_lines("##MemUsage3", Mem_usage_history[2], 0, "");
		
		imguigml_separator();
		ret = imguigml_input_int("Poll Frequency", meminfo_get_poll_frequency());
		if (ret[0]) 
			meminfo_set_poll_frequency(max(ret[1], 200));
	}
	imguigml_end();
	imguigml_set_window_size("Memory Usage", 400, 175, EImGui_Cond.Once);
}