/*
 * Copyright Adam Pritchard 2015
 * MIT License : http://adampritchard.mit-license.org/
 */

'use strict';
/*jshint node:true*/

function slugify(s, md) {
  // Unicode-friendly
  var spaceRegex = new RegExp(md.utils.lib.ucmicro.Z.source, 'g');
  return encodeURIComponent(s.replace(spaceRegex, ''));
}

function makeRule(md, options) {
  return function addHeadingAnchors(state) {
    // Go to length-2 because we're going to be peeking ahead.
    for (var i = 0; i < state.tokens.length-1; i++) {
      if (state.tokens[i].type !== 'heading_open' ||
          state.tokens[i+1].type !== 'inline') {
        continue;
      }

      var headingOpenToken = state.tokens[i];
      var headingInlineToken = state.tokens[i+1];

      let level = Number(headingOpenToken.tag.substr(1));
      if (level > 4)
        continue;

      if (!headingInlineToken.content) {
        continue;
      }
      
      var anchorHook = headingInlineToken.content;
      if (anchorHook[0] == '`')
        anchorHook = anchorHook.replace(/`/g, "").trim();
      
      anchorHook = options.slugify(anchorHook, md);
      
      if (options.addHeadingID) {
        state.tokens[i].attrPush(['id', anchorHook]);
      }

      if (options.addHeadingAnchor) {
        var anchorToken = new state.Token('html_inline', '', 0);
        anchorToken.content =
          '<a name="' + anchorHook + 
          '" href="#' + anchorHook + '">#</a> ';

        headingInlineToken.children.unshift(anchorToken);
      }

      // Advance past the inline and heading_close tokens.
      i += 2;
    }
  };
}

module.exports = function headinganchor_plugin(md, opts) {
  var defaults = {
    anchorClass: 'markdown-it-headinganchor',
    addHeadingID: true,
    addHeadingAnchor: true,
    slugify: slugify
  };
  var options = md.utils.assign(defaults, opts);
  md.core.ruler.push('heading_anchors', makeRule(md, options));
};
