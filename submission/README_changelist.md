
### CHANGES

v2.0.0
*  Added BorderlessToggle and MemInfo into outsideTheBox
*  Refactored and re-organized the entire API
*  Added `otb_browse_for_folder` 
*  Added `otb_filesystem_list_directory` and `otb_filesystem_path`

v0.10.1
*  Added Ubuntu support
*  Fixed inability to write _new_ files.

v0.9.4
*  Added `oTB_file_to_string` that writes a text file's contents to a string
*  Removed `oTB_Defines` and added it to oTB_init
*  Renamed `_oTB_findFile` to `__oTB_findFile` to keep consistent with code style
*  Documented function headers properly.