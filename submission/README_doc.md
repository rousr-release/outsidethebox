<link rel = "stylesheet" type = "text/css" href = "retro.css" />  

![outsideTheBox Banner](https://i.imgur.com/8kxxQQQ.png)
# outsideTheBox

outsideTheBox is a collection of system-level utility tools that didn't come built-in with GameMaker Studio 2.
It includes:

*   Saving / Loading files outside of the GameMaker Studio 2 sandbox
*   Toggling Borderless / Windowed modes at run-time
*   Querying detailed memory usage information

[Skip to Reference](#APIReference)

## **v2.0.0**

Version 2.0.0 introduces the meminfo and borderlessToggle rousr extensions all packed into one convenient package with outsideTheBox. outsideTheBox is written so that any unused features are not in any sort of "active" state at anytime, so if you're not interested in the additionally functionality, simply don't use it!

**Note: Many API Changes in this version. It is best to remove the old version first.**

### Installation Notes

*  ​Import Assets from the `outsideTheBox` groups in each Resource
*  Import Assets from the "(required)" marked groups (See note below)

**Note:** outsideTheBox uses functions from [rousrSuite](https://marketplace.yoyogames.com/assets/6319/rousrsuite). When importing assets from the extension, make sure to include any `rousr (required)` group. If you have any other [rousr](https://marketplace.yoyogames.com/publishers/3603/rousr) extensions, you only need to keep one copy of each of these resources. If you do import multiples, GameMaker Studio 2 will rename the duplicates with a `_1` at the end. You can simply delete any of the resources after importing the asset.

### Credits

*   by [@babyj3ans](http://babyjeans.rou.sr)
*   Additional Programming by [@net8floz](http://www.twitter.com/net8floz)
*   Uses:
    *  [imguigml](http://imguigml.rou.sr/) - Available also at [yy marketplace](https://marketplace.yoyogames.com/assets/6221/imguigml) or [itch.io](https://rousr.itch.io/imguigml)
    *  [rousrSuite (**yymarketplace**)](https://marketplace.yoyogames.com/assets/6319/rousrsuite) - General purpose scripts 

-----

## Components

outsideTheBox is made up of four components:

*   [File System](#FileSystem)
*   [Borderless Toggle](#BorderlessToggle)
*   [MemInfo](#MemInfo)

-----

### File System

Since it works by relocating files to your sandbox, then moving them back, it allows the users to still use all the same builtin file functions of GMS, i.e., `file_text_read_line`, etc

### Borderless Toggle

Using this extension, a GMS2 application can now toggle between a regular Windowed mode and Borderless Windowed mode.

Usage:

![Project Settings](http://static.rou.sr/outsideTheBox/borderless_toggle_settings.png)

* Set your project to use Borderless Windows, and make sure you don’t have “Allow player to resize” checked.
* Call `otb_set_borderless` and `otb_set_windowed` and off we go!

### MemInfo

The following functions can print out the various memory amounts:

```
show_debug_message("Memory Usage: ");
show_debug_message("\tPrivate Working Set (task manager, almost): " + string(meminfo_get_mem_usage()));
show_debug_message("\tWorking Set (physical memory):              " + string(meminfo_get_working_set_usage()));
show_debug_message("\tPrivate (virtual memory):                   " + string(meminfo_get_private_usage()));
```

**What this all means:**

it should be noted that the Private Working Set value is _as close_ as I could find to what TaskMgr gives us.
there's still some windows kernel overhead that isn't accounted for, because it's owned by the kernel.
... it also takes a second from boot to start getting accurate.
  
the Working Set is all the memory our process is using in addition to the amount of memory shared with other processes.
shared memory is memory used for things like shared DLLs (think the C++ Runtimes you have filling up your Add/Remove programs)
  
the 'private' is the virtual memory your process uses, it's similar to the private working set but is missing things like the executable image

----------------------

## API Reference

