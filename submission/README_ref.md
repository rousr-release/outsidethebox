#### `otb_file_open`   
| | | 
| ------------------------ | -------------------------------------------------- |   
**_file_path**             | {String} path to the file   
**[_file_flags=oTB.Read]** | {Real:oTB} the flags to use while opening. see oTB   
**returns**: |            {Real} _otb_file_handle or -1 on failure   
```
otb_file_open(_file_path, [_file_flags=oTB.Read])
```   
open a file... outside the box - tries to open the file outside the sandbox first, if it can't it'll try opening a sandboxed file.. _just in case_

---

#### `otb_file_close`   
| | | 
| -------------- | ------------------------------------------- |   
**_file_handle** | {Real} file handle returned by oTB_fileOpen   
**returns**: |  None   
```
otb_file_close(_file_handle)
```   
Close a file opened with otb

---

#### `otb_file_to_string`   
| | | 
| ---------- | ------------------------------------------------------------------------ |   
**_fname**   | {String} filename to open. will use file_text_open_read if it oTB fails.   
**returns**: | {String} undefined on failure, string of the file on success.   
```
otb_file_to_string(_fname)
```   
Open a files and returns it's lines as a string value

---

#### `__otb_init`   
**returns**: None   
```
__otb_init()
```   
initialze outsideTheBox and set the sandbox filepath

---

#### `__otb_file_find`   
| | | 
| ---------------------- | ----------------------------------------------- |   
**_file_path_or_handle** | {Real|String} Either a file path or file handle   
**returns**: |          {Array:EOTBFile} _file_data   
```
__otb_file_find(_file_path_or_handle)
```   
find an `EOTBFile` from a filename or GMS fileHandle

---

#### `otb_browse_for_folder`   
| | | 
| ------------------------------ | ------------------------------------------------------------------------------ |   
**[_default_path=""]**           | {String} default path to search browse from   
**[_title="Browse**              | {String} for folder..."] title to use for window   
**[_flags=BIF_RETURNONLYFSDIRS** | {Real:EOTBBrowseFile} | BIF_NEWDIALOGSTYLE] default path to search browse from   
**returns**: |                  {String} _path the user picked or undefined if error.   
```
otb_browse_for_folder([_default_path=""], [_title="Browse, [_flags=BIF_RETURNONLYFSDIRS)
```   
open a "browse for folder" windows dialog (Windows only)

---

#### `otb_browse_for_folder_async`   
| | | 
| ------------------------------ | ------------------------------------------------------------------------------ |   
**[_default_path=""]**           | {String} default path to search browse from   
**[_title="Browse**              | {String} for folder..."] title to use for window   
**[_flags=BIF_RETURNONLYFSDIRS** | {Real:EOTBBrowseFile} | BIF_NEWDIALOGSTYLE] default path to search browse from   
**returns**: |                  None   
```
otb_browse_for_folder_async([_default_path=""], [_title="Browse, [_flags=BIF_RETURNONLYFSDIRS)
```   
open a "browse for folder" windows dialog (Windows only). **Note:** On returning the path, an "Async Social" event is called with the `async_load` key `browse_for_folder` set to the path. _**Note:** Using this function twice will result in the second one blocking until the first is complete._

---

#### `otb_filesystem_path`   
| | | 
| ---------- | ----------------------------- |   
**_path**    | {String} path to get entry of   
**returns**: | {Array:EOTBFileSystemEntry} file entry for `_path`   
```
otb_filesystem_path(_path)
```   
get the file entry for path, or `undefined` if bad path

---

#### `otb_filesystem_list_directory`   
| | | 
| ---------- | ---------------------------------------- |   
**_path**    | {String} directory to get file list from   
**returns**: | {Array: [ EOTBFileSystemEntry, ... ] } list of files, or undefined if invalid path   
```
otb_filesystem_list_directory(_path)
```   
get the list of files and directories at a given path

---

#### `__otb_filesystem_read_entry`   
| | | 
| ---------- | --------------------------------------- |   
**_buffer**  | {Real} buffer id of buffer to read from   
**returns**: | {Array:EOTBFileSystemEntry} read entry or undefined for some reason   
```
__otb_filesystem_read_entry(_buffer)
```   
reads a path entry from a buffer [internal]

---

#### `otb_set_borderless`   
**returns**: None   
```
otb_set_borderless()
```   
Set the window to be borderless

---

#### `otb_set_windowed`   
**returns**: None   
```
otb_set_windowed()
```   
Set the window to have a frame

---

#### `__otb_preserve_window_size`   
**returns**: None   
```
__otb_preserve_window_size()
```   
internal function to make sure the window stays the appropriate size

---

#### `meminfo_start`   
| | | 
| ----------------------- | --------------------------------------- |   
**[_poll_frequency=200]** | {Real} how often to check the mem usage   
**returns**: |           None   
```
meminfo_start([_poll_frequency=200])
```   
start polling memory information - note: optional, calling a memory function calls this

---

#### `meminfo_stop`   
**returns**: None   
```
meminfo_stop()
```   
stop polling memory information - note: kills the thread, can help perf

---

#### `meminfo_get_poll_frequency`   
**returns**: {Real} frequency we poll memory   
```
meminfo_get_poll_frequency()
```   
start polling memory information

---

#### `meminfo_set_poll_frequency`   
| | | 
| ----------------- | --------------------------------------- |   
**_poll_frequency** | {Real} how often to check the mem usage   
**returns**: |     None   
```
meminfo_set_poll_frequency(_poll_frequency)
```   
set polling frequency

---

#### `meminfo_get_mem_usage`   
**returns**: {Real} physical memory usage   
```
meminfo_get_mem_usage()
```   
get the physical memory usage

---

#### `meminfo_get_private_usage`   
**returns**: {Real} private memory usage   
```
meminfo_get_private_usage()
```   
get the private memory (vmem) usage

---

#### `meminfo_get_working_set_usage`   
**returns**: {Real} private memory usage   
```
meminfo_get_working_set_usage()
```   
get the private working set memory

---

