For a few years, I've used a budget app I wrote in Ruby which I had the unfortunate loss of when we recently updated rou.sr to Ubuntu and plopped nodebb on it. So, I did what any sane programmer would do, and set out to re-write it in my current favorite tool... right now that happens to be: [GameMakerStudio 2](https://www.yoyogames.com/gamemaker/studio2) (I fricken love it).

One speed bump I've run into though, is that you can only save files within the sandbox. I understand this limitation, but for me, my app would be much more useful if I could choose to write to my dropbox. After mulling it over, I came up with a solution... write a DLL plugin!

## The Design

So - what my goal was here, was to write JSON text files to folders outside of the sandbox. I like using built-in functions of the tools I'm working with as much as possible, so it was preferable to allow the built-in GameMakerStudio2 file functions work. My first thought was:

 *Ok, well, can I just somehow return a filehandle and translate all the calls from GameMaker?*

**No.** This hardly even makes sense. Most everything in GameMaker is either a Real or a String, with complex types being represented by a Real that serves as an index... so thisgave me an idea.  I could write a 'wrapper' system - if you were to open a file for reading from outside, I can just copy it to the sandboxed save folder, and delete it on close. For writing to a file, write to your save folder, and copy it to the final location on close. 

Some simple GML functions can wrap it up for good housekeeping, and the C++ DLL handles the simple file negotiations.

### Potential Issues

* **File Size**: Obviously, this is a great idea for small files - but what about larger files? Well, I'm not making a general purpose system here... I'm trying to work with some JSON files so for me - that's fine. However, another solution may be to use something like [CreateSymbolicLink](https://msdn.microsoft.com/en-us/library/windows/desktop/aa363866(v=vs.85).aspx) and removing it when the file is closed.
* **Privileges**: I'm not sure how it'll behave if you try to access files you need administrator privileges for... not good I'm sure, not good indeed. I'm sure I can detect the lack of elevated privileges, but not sure about prompting to elevate.

## Alright Let's Get To It!

[Setup](#setup)

### Setup
I wasn't too familiar with creating DLLs, being a little rusty in that department, but after some quick reading up on it, I started simple... here's some steps to follow along with -

1) First [Visual Studio Community Edition](https://www.visualstudio.com/downloads/), and you're going to want to create a DLL project. 
**Note: I unchecked Create Directory for Solution, I suggest you do the same if you're following along**
![0_1486364300233_upload-e62a0da5-0d8b-4b2a-92c5-9eb477a3526f](/uploads/files/1486364302600-upload-e62a0da5-0d8b-4b2a-92c5-9eb477a3526f-resized.png) 

2) For Simplicity's sake, start with an empty project:
![0_1486364370178_upload-431d8185-8019-4148-88c3-9035fe98bd41](/uploads/files/1486364372276-upload-431d8185-8019-4148-88c3-9035fe98bd41-resized.png) 

3) Now, here's where I get a little strange... and to follow along you may want to do this as well... close Visual Studio, and find the Project folder in Explorer, to stay with me, and you'll see why later when we've got the GMS2 project, rename your project folder, *outsideTheBox* to **DLL**. And then, add a new **outsideTheBox** folder, and drag DLL into it:
![0_1486364723289_upload-11e4bb81-26bf-4767-a732-9cd36a576fe3](/uploads/files/1486364725437-upload-11e4bb81-26bf-4767-a732-9cd36a576fe3-resized.png) 

4) Open GameMakerStudio2, and create a new GML Language Project, and you'll want to put it inside the root folder (the one that contains DLL):
![0_1486364827890_upload-6a5bb8d1-c8fe-4f7a-842d-67b013b6932a](/uploads/files/1486364830068-upload-6a5bb8d1-c8fe-4f7a-842d-67b013b6932a-resized.png) 

And now you should have a folder structure setup like mine:

```
.\outsideTheBox\
.\outsideTheBox\outsideTheBox\
.\outsideTheBox\dll\
```

### The C++ Extensions

Navigate to your project file in the DLL folder, and open it up.

First, we need to implement the functionality we're looking for, and to do that we'll need us some `.cpp` files, of course. So let's add a `main.cpp` file to our project:
[image of adding a file]

Let's start off with writing the functions we want to connect to GML... we're going to need a file open and a file close at the very least:

```cpp
extern "C" {
	DECLAPI char* oTB_fileOpen(const char* _fileName) {
		return nullptr;
	}

	DECLAPI void oTB_fileClose(const char* _fileName) {

	}
}

```

So let's take a step back for a moment, and we'll go over each of these:

`extern 'C'` is telling the compiler to treat these as C-functions, which is ultimately important to our needs so that GMS is able to 'find' the functions by name. Without using this, you'll compiled the code as C++ which might sound like what you want - but in this case it'll also mangle the names on compile, making it much harder for external applications to find the functions in the DLL. Usually, you use import libraries (`.lib` files) and other techniques to deal with this when working with `cpp` libs, but GMS is happiest working with 32-bit C-api-like DLLs, so we'll just play along and use `extern 'C'` around the functions exposed to GML.

`DECLAPI` 