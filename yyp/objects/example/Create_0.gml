///@desc example - create
instance_create_depth(0, 0, depth-10, imgui);

Text_editor   = undefined;
Is_borderless = true;

Show_mem_usage = false;
Mem_usage_count       = 10;
Mem_usage_sample_time = 2;   // every 2 seconds add a plot
Mem_usage_next_sample = Mem_usage_sample_time * 1000000
Mem_usage_sample_index = 0;
Mem_usage_history = array_create(3);
for (var i = 0; i < 3; ++i)
	Mem_usage_history[@ i] = array_create(Mem_usage_count, 0);

Working_path = "";
List_dirs = false;

Working_file_index    = undefined;
Working_file          = undefined;
Working_file_handle   = undefined;
Working_file_contents = "";
Working_files_num     = undefined;
Working_file_names    = undefined;
Working_file_list     = undefined;
