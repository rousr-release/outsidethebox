///@func otb_set_borderless()
///@desc Set the window to be borderless
__otb_preserve_window_size();
__ext_otb_bt_set_borderless();