///@function meminfo_get_poll_frequency()
///@desc start polling memory information
///@returns {Real} frequency we poll memory
return __ext_otb_meminfo_get_poll_frequency();