///@function meminfo_start([_poll_frequency=1500])
///@desc start polling memory information - note: optional, calling a memory function calls this
///@param {Real} [_poll_frequency=200]   how often to check the mem usage
var _freq = argument_count > 0 ? argument[0] : undefined;
__ext_otb_meminfo_init();
if (_freq != undefined)
  __ext_otb_meminfo_set_poll_frequency(_freq);