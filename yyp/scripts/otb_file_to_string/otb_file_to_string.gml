///@function otb_file_to_string(_fname)
///@desc Open a files and returns it's lines as a string value 
///@param {String} _fname        filename to open. will use file_text_open_read if it oTB fails.
///@return {String} undefined on failure, string of the file on success.
var _dataPath  = argument_count >= 2 ? argument[0] : "";
var _fname     = argument_count >= 2 ? argument[1] : argument[0];

var _file = otb_file_open(_fname, oTB.Read),
	_string = "";

if (_file >= 0) {
	while (!file_text_eof(_file))
		_string += file_text_readln(_file);

	otb_file_close(_file);
}

return _string;