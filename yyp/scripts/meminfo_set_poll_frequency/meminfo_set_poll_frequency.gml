///@function meminfo_set_poll_frequency(_poll_frequency)
///@desc set polling frequency
///@param {Real} _poll_frequency   how often to check the mem usage
__ext_otb_meminfo_set_poll_frequency(argument0);