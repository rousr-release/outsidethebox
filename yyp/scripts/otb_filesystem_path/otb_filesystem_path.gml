///@function otb_filesystem_entry(_path)
///@desc get the file entry for path, or `undefined` if bad path
///@param {String} _path   path to get entry of
///@returns {Array:EOTBFileSystemEntry} file entry for `_path`
var _path = argument0;

var entryBuffer = buffer_create(_OTB_ENTRY_BUFFER_SIZE, buffer_fixed, 1);
var entry = undefined;
if (__ext_otb_filesystem_path(_path, buffer_get_address(entryBuffer), _OTB_ENTRY_BUFFER_SIZE) > 0)
  entry = __otb_filesystem_read_entry(entryBuffer);
buffer_delete(entryBuffer);

return entry;