///@function meminfo_get_mem_usage()
///@desc get the physical memory usage
///@returns {Real} physical memory usage
return __ext_otb_meminfo_get_private_working_set_mem_usage();