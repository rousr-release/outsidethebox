///@function __otb_init()
///@desc initialze outsideTheBox and set the sandbox filepath
gml_pragma("global", "global.___otbInitialized = false;");
gml_pragma("global", "__otb_init();");

if (global.___otbInitialized)
     return;

global.___otbInitialized = true;
global.___otbInitError   = true

global.___otbFiles       = sr_array_create();
global.___otbFreeIndices = sr_stack_array_create();



var cf   = file_text_open_write("otb.cfg");
var path = filename_path("otb.cfg");  // determine if we're %AppData% or %LocalAppData%
file_text_close(cf);

__ext_otb_bt_set_window_handle(window_handle());
global.___otbInitError = __ext_otb_set_app_data_path(path);
return global.___otbInitError;