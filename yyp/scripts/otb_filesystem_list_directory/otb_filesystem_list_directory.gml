///@function otb_fileystem_list_directory(_path)
///@desc get the list of files and directories at a given path
///@param {String} _path   directory to get file list from
///@returns {Array: [ EOTBFileSystemEntry, ... ] } list of files, or undefined if invalid path
var _path = argument0;
var folder_buffer = buffer_create(_OTB_ENTRY_BUFFER_SIZE, buffer_fixed, 1);
__ext_otb_filesystem_list_directory(_path, buffer_get_address(folder_buffer), _OTB_ENTRY_BUFFER_SIZE);

var contents = [ ], num_entries = 0;
var ret = buffer_peek(folder_buffer, 0, buffer_s8);
if (ret == -1)
	return undefined;

while (buffer_read(folder_buffer, buffer_s8) != 0) {
  var entry = __otb_filesystem_read_entry(folder_buffer);
  contents[@ num_entries++] = entry;
}

return contents;