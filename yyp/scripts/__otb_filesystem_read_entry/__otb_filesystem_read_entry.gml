///@function __otb_filesystem_read_entry(_buffer)
///@desc reads a path entry from a buffer [internal]
///@param {Real} _buffer   buffer id of buffer to read from
///@returns {Array:EOTBFileSystemEntry} read entry or undefined for some reason
var _buffer = argument0;
var entry = array_create(EOTBFileSystemEntry.Num);

entry[@ EOTBFileSystemEntry.Name]           = buffer_read(_buffer, buffer_string);
entry[@ EOTBFileSystemEntry.Ext]            = buffer_read(_buffer, buffer_string);
entry[@ EOTBFileSystemEntry.AbsolutePath]   = buffer_read(_buffer, buffer_string);
entry[@ EOTBFileSystemEntry.GenericPath]    = buffer_read(_buffer, buffer_string);
entry[@ EOTBFileSystemEntry.Accessible]     = buffer_read(_buffer, buffer_s8) != 0;
entry[@ EOTBFileSystemEntry.Size]           = buffer_read(_buffer, buffer_u32);
  
entry[@ EOTBFileSystemEntry.IsFolder]       = buffer_read(_buffer, buffer_s8) != 0;
entry[@ EOTBFileSystemEntry.IsEmpty]			  = buffer_read(_buffer, buffer_s8) != 0;
entry[@ EOTBFileSystemEntry.IsRegularFile]  = buffer_read(_buffer, buffer_s8) != 0;
entry[@ EOTBFileSystemEntry.IsSymLink]		  = buffer_read(_buffer, buffer_s8) != 0;
entry[@ EOTBFileSystemEntry.IsOther]			  = buffer_read(_buffer, buffer_s8) != 0;
	
entry[@ EOTBFileSystemEntry.IsAbsolutePath] = buffer_read(_buffer, buffer_s8) != 0;
entry[@ EOTBFileSystemEntry.IsRelativePath]	= buffer_read(_buffer, buffer_s8) != 0;
		
entry[@ EOTBFileSystemEntry.CreatedTime]    = buffer_read(_buffer, buffer_u64);
entry[@ EOTBFileSystemEntry.AccessedTime]   = buffer_read(_buffer, buffer_u64);
entry[@ EOTBFileSystemEntry.ModifiedTime]   = buffer_read(_buffer, buffer_u64);

return entry;