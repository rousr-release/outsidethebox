///@function meminfo_get_working_set_usage()
///@desc get the private working set memory 
///@returns {Real} private memory usage
return __ext_otb_meminfo_get_physical_mem_usage();