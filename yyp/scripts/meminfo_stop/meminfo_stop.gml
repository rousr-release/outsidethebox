///@function meminfo_stop()
///@desc stop polling memory information - note: kills the thread, can help perf
__ext_otb_meminfo_shutdown();