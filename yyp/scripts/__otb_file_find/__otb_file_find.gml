///@function __otb_file_find(_file_path_or_handle)
///@desc find an `EOTBFile` from a filename or GMS fileHandle
///@param {Real|String} _file_path_or_handle    Either a file path or file handle 
///@return {Array:EOTBFile} _file_data
var _file_path = argument0;
__otb_init();

var file_db   = global.___otbFiles;
var num_files = sr_array_size(file_db);
for (var i = 0; i < num_files; ++i) {
  var file_data = sr_array(file_db, i);
  if ( (is_string(_file_path) && file_data[EOTBFile.FilePath]  == _file_path) ||
       (is_real(_file_path)   && file_data[EOTBFile.GMLHandle] == _file_path)) {
    return file_data;     
  }
}

return undefined;
