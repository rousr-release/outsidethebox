///@function otb_file_close(_file_handle)
///@desc Close a file opened with otb
///@param {Real} _file_handle   file handle returned by oTB_fileOpen
var _fh = argument0;
__otb_init();

var file_data = __otb_file_find(_fh);
if (file_data == undefined) {
  show_debug_message("Unable to find file (handle: " + string(_fh) + ") to close! Data may be lost.");
  return;
}

var flags = file_data[EOTBFile.FileFlags];
var oh    = file_data[EOTBFile.OTBHandle];

if ((flags & oTB.Text) == oTB.Text || (flags & oTB.Binary) != oTB.Binary) 
  file_text_close(_fh);
else
  file_bin_close(_fh);

var fdb = global.___otbFiles;
sr_array_remove(fdb, sr_array_find(fdb, file_data));

if (oh >= 0)
	__ext_otb_file_close(oh);