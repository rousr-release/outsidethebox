///@function otb_browse_for_folder_async([_default_path=""], [_title="Browse for folder..."], 
///@desc open a "browse for folder" windows dialog (Windows only). **Note:** On returning the path, an "Async Social" event is called with the `async_load` key `browse_for_folder` set to the path. _**Note:** Using this function twice will result in the second one blocking until the first is complete._
///@param {String}              [_default_path=""]                                   default path to search browse from
///@param {String}              [_title="Browse for folder..."]                      title to use for window
///@param {Real:EOTBBrowseFile} [_flags=BIF_RETURNONLYFSDIRS | BIF_NEWDIALOGSTYLE]   default path to search browse from
var _default_path = argument_count > 0 ? argument[0] : "";
var _title        = argument_count > 1 ? argument[1] : "Browse for folder...";
var _flags        = argument_count > 2 ? argument[2] : (EOTBBrowseFile.BIF_RETURNONLYFSDIRS | EOTBBrowseFile.BIF_NEWDIALOGSTYLE);

__ext_otb_browse_for_folder(_default_path, _title, _flags, 1.0);