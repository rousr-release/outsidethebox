///@function meminfo_get_private_usage()
///@desc get the private memory (vmem) usage
///@returns {Real} private memory usage
return __ext_otb_meminfo_get_vmem_usage();