///@func __otb_preserve_window_size()
///@desc internal function to make sure the window stays the appropriate size
window_set_min_width(window_get_width());
window_set_min_height(window_get_height());

window_set_max_width(window_get_width());
window_set_max_height(window_get_height());