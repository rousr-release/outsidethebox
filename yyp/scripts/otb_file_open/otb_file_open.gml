///@function otb_file_open(_file_path, [_file_flags=oTB.Read])
///@desc open a file... outside the box - tries to open the file outside the sandbox first, if it can't it'll try opening a sandboxed file.. _just in case_
///@param {String}   _file_path               path to the file
///@param {Real:oTB} [_file_flags=oTB.Read]   the flags to use while opening. see oTB
///@returns {Real} _otb_file_handle or -1 on failure
var _file_path  = argument[0];
var _file_flags = argument_count > 1 ? argument[1] : oTB.Read;
__otb_init();

var fileData = __otb_file_find(_file_path);
if (fileData != undefined) {
  // error - it's already open?
  return fileData[EOTBFile.GMLHandle];
}

var is_binary = (_file_flags & oTB.Binary) == oTB.Binary,
		is_text   = (_file_flags & oTB.Text) == oTB.Text || !is_binary,
		is_write  = (_file_flags & oTB.Write) == oTB.Write,
		is_read   = (_file_flags & oTB.Read) == oTB.Read || !is_write,
		is_append = !is_read && (_file_flags & oTB.Append == oTB.Append);

var otb_handle = __ext_otb_file_open(_file_path, is_write || is_append);
var otb_failed = otb_handle < 0;

var otb_fname = otb_failed ? _file_path : __ext_otb_file_get_name(otb_handle);

  
if (is_text && is_binary) {
  show_debug_message("outsideTheBox: can't open file for both text and binary, binary used.");
	is_text = false;
}

var fh = -1;
		
if (is_text) {
	if (is_read && is_write) {
		show_debug_message("outsideTheBox: can't open text for reading and writing. opening for read.");
		is_write = false;
		is_append = false;
	}
	
	if (is_append)
    fh = file_text_open_append(otb_fname);
	else if (is_write)
		fh = file_text_open_write(otb_fname);
	else
    fh = file_text_open_read(otb_fname);
  
} else if (is_binary) {
	var mode = is_read ? 0 : undefined;
  if (is_write)
    mode = is_undefined(mode) ? 1 : 2;
    
  fh = file_bin_open(otb_fname, mode);
  
	// if we're not appending, clear the file
  if (is_append)
    file_bin_rewrite(fh);
}

if (fh < 0) {
	if (otb_handle >= 0)
		__ext_otb_file_close(otb_handle);
  show_debug_message("Unable to open AppData local file for " + string(otb_fname) + " (" + string(_file_path) + ")");
  return -1;
}

var fileDb      = global.___otbFiles;  

fileData = array_create(EOTBFile.Num);
fileData[@ EOTBFile.FilePath]  = _file_path;
fileData[@ EOTBFile.GMLFile]   = otb_fname;
fileData[@ EOTBFile.GMLHandle] = fh;	
fileData[@ EOTBFile.OTBHandle] = otb_handle;
fileData[@ EOTBFile.FileFlags] = _file_flags;
sr_array_push_back(fileDb, fileData);

return fh;