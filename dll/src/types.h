////
// types.h
//  babyjeans
//
// Common headers and types that all code uses.
////
#pragma once

#include "dllapi.h"

////
// C++
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <cstring>
#include <string>
#include <fstream>
#include <iostream>

////
// Platform
#ifdef WIN32
#include <Windows.h>
#endif 

////
// STL
#include <memory>
#include <array>
#include <string>
#include <vector>
#include <map>
#include <queue>
#include <set>
#include <unordered_set>
#include <stack>

#include <tuple>
#include <functional>
#include <thread>
#include <mutex>
#include <condition_variable>

#include <atomic>

#if defined __GNUC__
#include <unistd.h>
#include <wordexp.h>
#endif

#if defined __APPLE__
#include <mach-o/dyld.h>
#endif


////
// Constants
#define size_t_max      (static_cast<size_t>(~0))

#if defined (__GNUC__)
#define strncpy_s(a, b, c) strncpy(a, b, c)
#define MAX_PATH (256)
#endif 

////
// Spinlock
// courtesy: anki3d.org/spinlock
class SpinLock {
public:
    void lock() {
        while (mLock.test_and_set(std::memory_order_acquire)) { ; }
    }

    void unlock() {
        mLock.clear(std::memory_order_release);
    }

private:
    std::atomic_flag mLock = ATOMIC_FLAG_INIT;
};