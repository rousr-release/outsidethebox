////
// meminfo.h
//  babyjeans
//
// MemInfo is a windows-only portion of the extension that gets various
// memory usages data.
/////
#pragma once

#include <psapi.h>

class MemInfo {
public:
    MemInfo();
    virtual ~MemInfo();

    void SetPollFrequency(int _ms) { mPollFrequency = _ms; }
    int GetPollFrequency() const   { return mPollFrequency.load(); }

    double GetPhysMemUsage() const    { return mPhysMemUsage.load(); }
    double GetVMemUsage() const       { return mVMemUsage.load(); }
    double GetPrivateMemUsage() const { return mPrivateMemUsage.load(); }
private:
    void GetMemInfo();
    void FreePV();

private:
    // Store the values in atomics for querying from GML
    std::atomic<double> mPhysMemUsage    = 0.0;
    std::atomic<double> mVMemUsage       = 0.0;
    std::atomic<double> mPrivateMemUsage = 0.0;

    std::atomic<int>    mPollFrequency = 1500;  // How long, in MS, to sleep
    std::atomic<bool>   mShuttingDown  = false;
    std::atomic<bool>   mShutdown      = true;

    DWORD                          mProcessId = 0;
    PSAPI_WORKING_SET_INFORMATION* mpPv       = nullptr;
    BYTE*                          mMemBuffer = nullptr;
    size_t                         mCb        = sizeof(PSAPI_WORKING_SET_INFORMATION);
    SYSTEM_INFO                    mSi;
};
