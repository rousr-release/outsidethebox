////
// dllapi.h
//  babyjeans
//
// Macros to wrap up DLL attributes for easy function exporting
/////
#pragma once

#if defined(_MSC_VER)
#define DLL_EXPORT extern "C" __declspec(dllexport)  
#define DLL_IMPORT __declspec(dllimport)
#elif defined(__GNUC__)
#define DLL_EXPORT __attribute__((visibility("default")))
#define DLL_IMPORT 
#else
#define DLL_EXPORT
#define DLL_IMPORT
#endif

#if defined DLL_EXPORTS
#define DLL_API DLL_EXPORT
#else
#define DLL_API DLL_IMPORT
#endif
