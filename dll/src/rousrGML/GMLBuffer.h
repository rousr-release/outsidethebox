#pragma once

#include "GMLBufferBase.h"

class CGMLBuffer : public CGMLBufferBase
{
public:
    CGMLBuffer(size_t _bufferSize = 0, const char* _bufferPtr = nullptr) : CGMLBufferBase(_bufferSize, _bufferPtr) { ; }
    virtual ~CGMLBuffer() { ; }

    template <typename T, typename=std::enable_if_t<!std::is_same<T, std::string>::value>>
	void Write(const T& _val) {
        size_t totalSize(mSeekPos + sizeof(T));
		if (totalSize >= mBufferSize)
			return;

        T& val = *reinterpret_cast<T*>(&mBuffer[mSeekPos]);
        val = _val;
		mSeekPos += sizeof(T);
	}
	
    template <typename T, typename=std::enable_if_t<std::is_same<T, std::string>::value>>
	void Write(const std::string& _val) {
        size_t length(_val.length());
        if (mSeekPos + length + 1 >= mBufferSize)
            return;

        char* _buffer = &mBuffer[mSeekPos];
        std::memcpy(_buffer, _val.c_str(), length);
        mSeekPos += _val.length();
        mBuffer[mSeekPos++] = 0;
	}
};
