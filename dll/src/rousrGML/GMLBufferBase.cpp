#include "GMLBufferBase.h"

CGMLBufferBase::CGMLBufferBase(size_t _bufferSize, const char* _bufferPtr)
	: mBuffer(const_cast<char*>(_bufferPtr))
	, mSeekPos(0)
	, mBufferSize(_bufferSize)
{ }

int8_t CGMLBufferBase::PeekByte() const {
	return mBuffer[mSeekPos];
}

int8_t CGMLBufferBase::PeekByte(uint32_t _seekPos) const {
	return mBuffer[_seekPos];
}

int8_t CGMLBufferBase::ReadByte() {
    if (mSeekPos + 1 >= mBufferSize)
        return mBuffer[mSeekPos];

	return mBuffer[mSeekPos++];
}

void CGMLBufferBase::Clear() { 
    mSeekPos = mBufferSize = 0; 
    mBuffer = nullptr; 
}

void CGMLBufferBase::WriteData(const char *_data, size_t _size) {
    std::memcpy(&mBuffer[mSeekPos], _data, _size);
    mSeekPos += _size;
}
