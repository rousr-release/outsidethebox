////
// exports.cpp
//  babyjeans
//
// Export all of the external functions for the DLL
/////
#include "types.h"

#include "otb.h"
#include "meminfo.h"

#include "rousrGML/GMLBuffer.h"
#include "rousrGML/GMLDSMap.h"

// Browse For Folder
#include <ShlObj.h>

namespace {
    std::unique_ptr<OutsideTheBox> outsideTheBox;
    void EnsureOTB() {
        if (outsideTheBox != nullptr)
            return;
        
        outsideTheBox = std::make_unique<OutsideTheBox>();
    }

    OutsideTheBox& GetOTB() {
        EnsureOTB();
        return *outsideTheBox;
    }
}
#define oTB  (GetOTB())

///////////////
// Function Exports
DLL_API double oTB_SetAppDataPath(const char *_appPath) {
    oTB.SetAppDataPath(_appPath);
    return 1.0;
}

DLL_API double oTB_FileOpen(const char* _filename, double _createIfMissing) {
    size_t ret(oTB.FileOpen(_filename, _createIfMissing > 0.0));
    int signedRet = static_cast<int>(ret);
    if (signedRet < 0)
        return static_cast<double>(signedRet);

    double fileHandle = static_cast<double>(ret);
    return fileHandle;
}

DLL_API const char* oTB_FileGetName(double _fileIndex) {
    return oTB.FileGetName(static_cast<size_t>(_fileIndex)).c_str();
}

DLL_API double oTB_FileClose(double _fileIndex) {
    oTB.FileClose(static_cast<size_t>(_fileIndex));
    return 1.0;
}

DLL_API double oTB_FileSystemPath(const char* _path, const char* _buffer, double _bufferSize) {
    return oTB.FileSystemPath(_path, CGMLBuffer(static_cast<size_t>(_bufferSize), _buffer)) ? 1.0 : -1.0;
}

DLL_API double oTB_FileSystemListDirectory(const char* _path, const char* _buffer, double _bufferSize) {
    oTB.FileSystemListDirectory(_path, CGMLBuffer(static_cast<size_t>(_bufferSize), _buffer));
    return 1.0;
}

DLL_API const char* oTB_BrowseForFolder(const char* _defaultPath, const char* _title, double _flags, double _async) {
    static char path[MAX_PATH];
    static char defaultPath[MAX_PATH];
    static char title[MAX_PATH];
    
    static SpinLock spinLock;

    // only allow once at a time
    spinLock.lock();
    
    path[0]        = 0;
    defaultPath[0] = 0;

    uint32_t flags(static_cast<uint32_t>(_flags));
    bool async(_async != 0.0);
    
    auto defaultLen(std::strlen(_defaultPath));
    auto titleLen(std::strlen(_title));

    if (defaultLen > 0)
        strncpy_s(defaultPath, MAX_PATH, _defaultPath, defaultLen);
    
    if (titleLen > 0)
        strncpy_s(title, MAX_PATH, _title, titleLen);

    auto thread(std::thread([flags, async] {

        BROWSEINFO bi = { 0 };
        bi.lpszTitle = title; // ("Browse for folder...");
        bi.ulFlags   = flags;  // BIF_RETURNONLYFSDIRS | BIF_NEWDIALOGSTYLE;
        bi.lParam    = reinterpret_cast<LPARAM>(defaultPath);
        bi.lpfn = [](HWND _hwnd, UINT _msg, LPARAM, LPARAM _data) {
            switch (_msg) {
            case BFFM_INITIALIZED: ::SendMessage(_hwnd, BFFM_SETSELECTION, true, _data); break;
            default: break;
            }

            return 0;
        };

        LPITEMIDLIST pidl(SHBrowseForFolder(&bi));
        if (pidl != 0) {
            //get the name of the folder and put it in path
            SHGetPathFromIDList(pidl, path);

            //free memory used
            IMalloc* imalloc(nullptr);
            if (SUCCEEDED(SHGetMalloc(&imalloc))) {
                imalloc->Free(pidl);
                imalloc->Release();
            }

            // send async message with path
            if (async) {
                CGMLDSMap asyncMsg;
                asyncMsg.AddString("browse_for_folder", path);
                asyncMsg.TriggerAsyncEvent();
            }
        }
        else if (async) {
            // send async message with empty 
            if (async) {
                CGMLDSMap asyncMsg;
                path[0] = 0;
                asyncMsg.AddString("browse_for_folder", path);
                asyncMsg.TriggerAsyncEvent();
            }

        }

        spinLock.unlock();
    }));

    if (_async == 0.0) {
        thread.join();
    } else
        thread.detach();
    
    return path;
}

DLL_API const char* oTB_GetAppDataPath() {
    return oTB.GetAppDataPath().c_str();
}

DLL_API const char* oTB_GetExecutablePath() {
    return oTB.GetExecutablePath().c_str();
}

DLL_API double BorderlessToggle_SetWindowHandle(char *_windowHandle) {
    oTB.SetWindowHandle(reinterpret_cast<HWND>(_windowHandle));
    return 1.0;
}

DLL_API double BorderlessToggle_SetBorderless()
{
    return oTB.SetBorderless() ? 1.0 : -1.0;
}

DLL_API double BorderlessToggle_SetWindowed()
{
    return oTB.SetWindowed() ? 1.0 : -1.0;
}

DLL_API double MemInfo_Init() {
    oTB.EnsureMemInfo();
    return 1.0;
}

DLL_API double MemInfo_Shutdown() {
    oTB.KillMemInfo();
    return 0.0;
}

DLL_API double MemInfo_SetPollFrequency(double _ms) {
    auto& memInfo(oTB.GetMemInfo());
    memInfo.SetPollFrequency(static_cast<int>(_ms));
    return 1.0;
}

DLL_API double MemInfo_GetPollFrequencey() {
    auto& memInfo(oTB.GetMemInfo());
    return static_cast<double>(memInfo.GetPollFrequency());
}

DLL_API double MemInfo_GetPrivateWorkingSetMemUsage() {
    auto& memInfo(oTB.GetMemInfo());
    return memInfo.GetPrivateMemUsage();
}

DLL_API double MemInfo_GetPhysicalMemUsage() {
    auto& memInfo(oTB.GetMemInfo());
    return memInfo.GetPhysMemUsage();
}

DLL_API double MemInfo_GetVMemUsage() {
    auto& memInfo(oTB.GetMemInfo());
    return memInfo.GetVMemUsage();
}

DLL_API double RousrURL(const char* _url) {
    ShellExecuteA(nullptr, nullptr, _url, nullptr, nullptr, SW_SHOW);
    return 1.0;
}