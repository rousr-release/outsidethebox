////
// otb.h
//  babyjeans
//
// Main OutsideTheBox header for the DLL
//  OutsideTheBox contains 3 extensions:
//   *  The original outsideTheBox: a simple extension that reads/writes files outside of the gamemaker sandbox
//   *  Borderless Toggle: a gms2 specialty for toggling borderless and bordered windowed modes
//   *  MemInfo: a simple utility that reads memory usage data for the proecss
//
//  This class is basically all three, though meminfo is split into its own object since it's a bit convoluted.
////
#pragma once  

namespace boost { namespace filesystem { class path; class directory_iterator; } }

class MemInfo;
class CGMLBuffer;

class OutsideTheBox {
public:
    static const std::string None;

public:
    OutsideTheBox(const std::string& _appDataPath = None) { SetAppDataPath(_appDataPath); }
    virtual ~OutsideTheBox() {}

    static std::string        GetExpandedPath(const std::string& _path);
    static const std::string& GetExecutablePath();
    
    void                      SetAppDataPath(const std::string& _path);
    const std::string&        GetAppDataPath() const { return mPath; }
    
    size_t             FileOpen(const std::string& _fileName, bool _createIfMissing);
    void               FileClose(size_t   _fileIndex);
    const std::string& FileGetName(size_t _fileIndex);

private:
    std::string                                       mPath = "";
    std::string                                       mAppPath = "";
    std::vector<std::tuple<std::string, std::string>> mFiles;

//////////
// FileSystem
public:
    bool FileSystemPath(const std::string& _path, CGMLBuffer& _out);
    void FileSystemListDirectory(const std::string& _path, CGMLBuffer& _out);

private:
    void FileSystemWritePath(const boost::filesystem::path& _path, CGMLBuffer& _out);


//////////
// Borderless Toggle
public:
    void SetWindowHandle(HWND _hwnd) { mHWND = _hwnd; }
    bool SetBorderless();
    bool SetWindowed();
    
    void SetAlwaysOnTop(bool _enabled);
    bool GetAlwaysOnTop() const { return mAlwaysOnTop; }

private:
    void FindWindowHandle();
    bool CheckSize();

    HWND mHWND = nullptr;
    int mWidth = 0;
    int mHeight = 0;
    bool mAlwaysOnTop = false;
    bool mBorderless  = false;

//////////
// MemInfo
public:
    void EnsureMemInfo();
    void KillMemInfo();
    MemInfo& GetMemInfo() { return *mMemInfo.get(); }

private:
    std::unique_ptr<MemInfo>  mMemInfo;
};

