////
// otb.cpp
//  babyjeans
//
// Main OutsideTheBox header for the DLL
//  OutsideTheBox contains 3 extensions:
//   *  The original outsideTheBox: a simple extension that reads/writes files outside of the gamemaker sandbox
//   *  Borderless Toggle: a gms2 specialty for toggling borderless and bordered windowed modes
//   *  MemInfo: a simple utility that reads memory usage data for the proecss
//
//  This class is basically all three, though meminfo is split into its own object since it's a bit convoluted.
////
#include "types.h"

#include "otb.h"
#include "meminfo.h"

#include "rousrGML/GMLBuffer.h"

#define BOOST_FILESYSTEM_NO_DEPRECATED
#include <boost/filesystem.hpp>
#include <sys/stat.h>

const std::string OutsideTheBox::None = "none";

namespace {
    std::string ExecutablePath = "";

    // Since we can't capture in the WNDENUMPROC callback, we'll pass this struct as the LPARAM.
    struct enumProcData {
        DWORD processID;
        OutsideTheBox* win;
        enumProcData(DWORD _id, OutsideTheBox* _pWin) : processID(_id), win(_pWin) { ; }
    };

};

///////////////
// AppRoot / Paths

///@function GetExpandedPath(_path)
///@desc expand a relative path and/or a path with environment variables in it.
///@param {const std::string&} _path   relative path / path with environment vars
///@returns {std::string} new string with expanded, absolute path
std::string OutsideTheBox::GetExpandedPath(const std::string& _path) {
    const int cnSize(1024);
    char expandedPath[cnSize];
    memset(expandedPath, 0, cnSize);
    std::string ret(expandedPath);
#if defined WIN32
    ExpandEnvironmentStringsA(_path.c_str(), expandedPath, cnSize);
    ret = expandedPath;
#elif defined __GNUC__
    wordexp_t p;
    wordexp(_path.c_str(), &p, 0);
    char** w(p.we_wordv);
    ret = "";
    for (size_t i(0); i < p.we_wordc; ++i) {
        if (i != 0)
            ret += " ";
        ret += w[i];
    }
#endif 
    return ret;
}

///@function GetExecutablePath()
///@desc get the path of the executable for this process
///@returns {const std::string&} Path to the executable that launched this DLL
const std::string& OutsideTheBox::GetExecutablePath() {
    if (ExecutablePath.empty() || ExecutablePath == "") {
        char buffer[MAX_PATH];

#if defined WIN32
        GetModuleFileNameA(NULL, buffer, MAX_PATH);
#elif defined __GNUC__

#if defined __APPLE__
        uint32_t buffSize(MAX_PATH);
        _NSGetExecutablePath(buffer, &buffSize);
#else 
        char procId[MAX_PATH];
        memset(procId, 0, MAX_PATH);
        sprintf(procId, "/proc/%d/exe", getpid());
        readlink(procId, buffer, MAX_PATH);
#endif
#endif 
        std::string buffString(buffer);
        std::string::size_type pos(buffString.find_last_of("\\/"));

        ExecutablePath = buffString.substr(0, pos);

    }

    return ExecutablePath;
}

///////////////
// FileSystem Functions

///@function OutsideTheBox::SetAppDataPath(_appDataPath)
///@desc set the path to where the running GMS2 app's sandbox is
///@param {const std::string&}[_appDataPath="none"]   the path to app data, if "none" is passed, uses executable path
void OutsideTheBox::SetAppDataPath(const std::string& _sPath) {
    FindWindowHandle();
    mPath = GetExpandedPath(_sPath == "none" ? GetExecutablePath() : _sPath);
}

///@function FileOpen(_filePath, _createIfMissing)
///@desc Open the the file at the path, optionally creating it if it's not there
///@param {const std::string&} _filePath   file path of file to open
///@param {bool} _createIfMissing          create the file if it's not there
///@returns {size_t} index of file that is open, -1 if can't open, -2 if can't write to sandbox
size_t OutsideTheBox::FileOpen(const std::string& _filePath, bool _createIfMissing) {
    auto expandedPath(GetExpandedPath(_filePath));
    std::ifstream readFile(expandedPath, std::ios::binary);
    if (!_createIfMissing && !readFile.is_open())
        return -1;

    std::string sTempFile = _filePath.substr(_filePath.find_last_of("\\/"));

    sTempFile = mPath + sTempFile.substr(sTempFile.find_first_not_of("\\/"));
    mFiles.push_back(std::make_tuple(expandedPath, sTempFile));

    if (!readFile.is_open()) {
        std::ofstream newFile(expandedPath, std::ios::binary);
        if (!newFile.is_open())
            return -1;
    }

    std::ofstream writeFile(sTempFile, std::ios::binary);
    if (!writeFile.is_open()) {
        mFiles.pop_back();
        return -2;
    }

    if (readFile.is_open()) {
        std::istreambuf_iterator<char> beginRead(readFile);
        std::istreambuf_iterator<char> endRead;
        std::ostreambuf_iterator<char> beginWrite(writeFile);
        std::copy(beginRead, endRead, beginWrite);
    }
    return mFiles.size() - 1;
}

///@function FileClose(_fileIndex)
///@desc Finish writing/reading the file, remove it from the sandbox and update the original file if necessary
///@param {size_t} _fileIndex   index of the file to look up
void OutsideTheBox::FileClose(size_t _szIndex) {
    if (_szIndex < mFiles.size()) {
        const auto& fileEntry(mFiles[_szIndex]);
        const std::string& sFilepath(std::get<0>(fileEntry));
        const std::string& sTempFile(std::get<1>(fileEntry));

        try
        {
            std::ifstream tempFile(sTempFile, std::ios::binary);
            std::ofstream realFile(sFilepath, std::ios::binary);
            realFile << tempFile.rdbuf();
        }
        catch (std::exception& e) {
            std::cerr << e.what();
        }

        std::remove(sTempFile.c_str());
        mFiles.erase(mFiles.begin() + _szIndex);
    }
}

///@function FileGetName(_fileIndex)
///@desc get the string name for a file
///@param {size_t} _fileIndex   index of the file to look up
///@returns {const std::string&} file name at the file index
const std::string&  OutsideTheBox::FileGetName(size_t _fileIndex) {
    if (_fileIndex < mFiles.size()) {
        return std::get<1>(mFiles[_fileIndex]);
    }

    return None;
}

///@funtion FileSystemPath
///@desc return a filesystem entry's data, file info / folder
///@param {const std::string&} _path   path 
///@returns {bool} true if the path is valid
bool OutsideTheBox::FileSystemPath(const std::string& _path, CGMLBuffer& _buffer) {
    boost::filesystem::path path(_path);
    if (!boost::filesystem::exists(path))
        return false;

    try {
        FileSystemWritePath(_path, _buffer);
    } catch (boost::filesystem::filesystem_error&) {
        return false; // nothing
    }

    return true;
}

///@function FileSystemListDirectory(_path)
///@desc Writes directory entries to a buffer
///@param {const std::string&} _path
void OutsideTheBox::FileSystemListDirectory(const std::string& _path, CGMLBuffer& _buffer) {
    _buffer.Seek(0);
    boost::filesystem::path path(_path);
    if (!boost::filesystem::exists(path)) {
        _buffer.Write<int8_t>(-1);
        return;
    }

    try {
        for (boost::filesystem::directory_iterator fileIter(path), end; fileIter != end; ++fileIter) {
            // write entry data
            auto& f(*fileIter);
            auto p(f.path());

            _buffer.Write<int8_t>(1);
            FileSystemWritePath(p, _buffer);
        }
    } catch (boost::filesystem::filesystem_error&) {
        _buffer.Write<int8_t>(-1);
    }
    _buffer.Write<int8_t>(0);
}

///@function FileSystemWritePath(_path)
///@desc write a _path entry to a rousr buffer
///@param {boost::filesystem::path} _path   `_path` to write
void OutsideTheBox::FileSystemWritePath(const boost::filesystem::path& _path, CGMLBuffer& _buffer) {
    auto fws = [&_buffer](const wchar_t* _wstring) {
        size_t numChars(0);
        char stringBuff[MAX_PATH];
        wcstombs_s(&numChars, stringBuff, _wstring, MAX_PATH);
        return std::string(stringBuff);
    };
    
    auto canonPath(boost::filesystem::canonical(_path));
    auto filePath(fws(canonPath.make_preferred().c_str()));
    auto genericPath(_path.generic_path());

    struct stat stat_data;
    memset(&stat_data, 0, sizeof(stat_data));
    auto ret(stat(filePath.c_str(), &stat_data));
    bool accessible(ret >= 0);

    _buffer.Write<std::string>(fws(_path.filename().c_str()));
    _buffer.Write<std::string>(_path.has_extension() ? fws(_path.extension().c_str()) : "");
    _buffer.Write<std::string>(filePath);
    _buffer.Write<std::string>(fws(genericPath.c_str()));
    _buffer.Write<int8_t>(accessible ? 1 : 0);
    _buffer.Write<uint32_t>(stat_data.st_size);
    
    _buffer.Write<int8_t>(boost::filesystem::is_directory(_path)    ? 1 : 0);
    _buffer.Write<int8_t>(accessible && boost::filesystem::is_empty(_path)        ? 1 : 0);
    _buffer.Write<int8_t>(boost::filesystem::is_regular_file(_path) ? 1 : 0);
    _buffer.Write<int8_t>(boost::filesystem::is_symlink(_path)      ? 1 : 0);
    _buffer.Write<int8_t>(boost::filesystem::is_other(_path)        ? 1 : 0);
    
    _buffer.Write<int8_t>(_path.is_absolute() ? 1 : 0);
    _buffer.Write<int8_t>(_path.is_relative() ? 1 : 0);
    
    _buffer.Write<uint64_t>(stat_data.st_ctime); // created time
    _buffer.Write<uint64_t>(stat_data.st_atime); // access time
    _buffer.Write<uint64_t>(stat_data.st_mtime); // modified time   
}

///////////////
// Borderless Toggle

///@function SetBorderless()
///@desc Sets the window into "borderless" windowed mode
bool OutsideTheBox::SetBorderless()
{
    if (!CheckSize())
        return false;

    mBorderless = true;
    SetWindowLongPtr(mHWND, GWL_STYLE, WS_VISIBLE | WS_POPUP);  // WS_POPUP removes the border
    SetWindowPos(mHWND, mAlwaysOnTop ? HWND_TOPMOST : NULL, 0, 0, mWidth, mHeight, SWP_NOMOVE | SWP_NOZORDER | SWP_FRAMECHANGED);

    return true;
}

///@function SetWindowed()
///@desc Sets the window into "bordered" windowed mode
bool OutsideTheBox::SetWindowed()
{
    if (!CheckSize())
        return false;

    mBorderless = false;
    SetWindowLongPtr(mHWND, GWL_STYLE, WS_VISIBLE | WS_OVERLAPPEDWINDOW); // WS_OVERLAPPED is our traditional frame style
    SetWindowPos(mHWND, mAlwaysOnTop ? HWND_TOPMOST : NULL, 0, 0, mWidth, mHeight, SWP_NOMOVE | SWP_NOZORDER | SWP_FRAMECHANGED);

    return true;
}

void OutsideTheBox::SetAlwaysOnTop(bool _enabled)
{
    mAlwaysOnTop = _enabled; 
    if (!CheckSize())
        return;
    
    SetWindowPos(mHWND, mAlwaysOnTop ? HWND_TOPMOST : NULL, 0, 0, mWidth, mHeight, SWP_NOMOVE | SWP_NOZORDER | SWP_FRAMECHANGED);
}

///@function Checksize()
///@desc Checks the current window size so that we can reset it properly after switching border style
///@returns {bool} false if it was unable to complete and calculate the size.
bool OutsideTheBox::CheckSize() {
    if (mHWND == NULL) {
        FindWindowHandle();
        if (mHWND == NULL)
            return false;
    }

    if (mHWND != NULL) {
        HWND parent(GetParent(mHWND));
        while (parent != NULL && parent != mHWND) {
            CloseWindow(mHWND);
            mHWND = parent;
            parent = GetParent(mHWND);
        }
    }

    RECT rcClient, rcWindow;
    GetClientRect(mHWND, &rcClient);
    GetWindowRect(mHWND, &rcWindow);
    mWidth = rcClient.right;
    mHeight = rcClient.bottom;

    if ((GetWindowLongPtr(mHWND, 0) & WS_OVERLAPPEDWINDOW) == WS_OVERLAPPEDWINDOW) {
        int w = rcWindow.right - rcWindow.left;
        int h = rcWindow.bottom - rcWindow.top;

        mWidth = w - mWidth;
        mHeight = h - mHeight;
    }

    return true;
}

///@function FindWindowHandle()
///@desc brute force way to find our window handle... also could just be passed.
void OutsideTheBox::FindWindowHandle() {
    enumProcData data(GetCurrentProcessId(), this);	// Get the current running processID, the GM game executable
    EnumWindows([](HWND _hWnd, LPARAM lParam)->BOOL {	// Loop through each top level window
        enumProcData& data = *reinterpret_cast<enumProcData*>(lParam);
        DWORD processId = data.processID;
        OutsideTheBox* fakeThis = data.win;
        DWORD windowProcessId = 0;
        GetWindowThreadProcessId(_hWnd, &windowProcessId); // Is this hwnd the one that matches our executable?
        if (windowProcessId == processId) {
            fakeThis->SetWindowHandle(_hWnd);
            return FALSE; // we're done, stop looping
        }

        return TRUE;
    }, reinterpret_cast<LPARAM>(&data));
}

///////////////
// Mem Info
void OutsideTheBox::EnsureMemInfo() {
    if (mMemInfo != nullptr) return;
    mMemInfo = std::make_unique<MemInfo>();
}

void OutsideTheBox::KillMemInfo() {
    mMemInfo.reset();
}