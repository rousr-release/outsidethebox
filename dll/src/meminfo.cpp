////
// meminfo.cpp
//  babyjeans
//
// MemInfo is a windows-only portion of the extension that gets various
// memory usages data.
/////
#include "types.h"

#include "meminfo.h"

namespace {
    const int MaxPollFrequency = 200;
}

///@function MemInfo()
///@desc constructor
MemInfo::MemInfo() {
    mProcessId = GetCurrentProcessId();
    mShutdown = false;
    
    std::thread([this] {
        std::memset(&mSi, 0, sizeof(SYSTEM_INFO));
        GetSystemInfo(&mSi);

        while (!mShuttingDown) {
            GetMemInfo();

            // Don't sleep too long if we haven't got our data yet.
            if (mPrivateMemUsage == 0.0) std::this_thread::yield();
            else std::this_thread::sleep_for(std::chrono::milliseconds(max(mPollFrequency.load(), MaxPollFrequency)));
        }
        mShutdown = true;
    }).detach();
}

///@function ~MemInfo()
///@desc destructor
MemInfo::~MemInfo() {
    mShuttingDown = true;
    while (!mShutdown)
        std::this_thread::yield();

    FreePV();
    mPhysMemUsage = 0.0;
    mVMemUsage = 0.0;
    mPrivateMemUsage = 0.0;
}

///@function GetMemInfo
///@desc Retrieve all the various memory infos.
void MemInfo::GetMemInfo() {
    auto getEntries = [this]() {
        auto& _pv(*mpPv);
        uint32_t totalPrivate(0u);
        MEMORY_BASIC_INFORMATION mbi;

        for (uint32_t i = 0; i < _pv.NumberOfEntries; ++i) {
            auto set(_pv.WorkingSetInfo[i]);
            std::memset(&mbi, 0, sizeof(MEMORY_BASIC_INFORMATION));
            VirtualQuery(reinterpret_cast<LPCVOID>(mSi.dwPageSize * set.VirtualPage), &mbi, sizeof(MEMORY_BASIC_INFORMATION));
            if (mbi.State == MEM_COMMIT && mbi.Type == MEM_PRIVATE)
                totalPrivate += static_cast<uint32_t>(mSi.dwPageSize);
        }

        mPrivateMemUsage = static_cast<double>(totalPrivate);
    };

    if (mpPv != nullptr && QueryWorkingSet(GetCurrentProcess(), mpPv, mCb)) {
        getEntries();
    } else {
        PSAPI_WORKING_SET_INFORMATION pv;
        std::memset(&pv, 0, sizeof(PSAPI_WORKING_SET_INFORMATION));
        if (!QueryWorkingSet(GetCurrentProcess(), &pv, sizeof(pv)) && GetLastError() == ERROR_BAD_LENGTH) {
            if (mpPv != nullptr)
                FreePV();

            mCb = sizeof(PSAPI_WORKING_SET_INFORMATION) + ((pv.NumberOfEntries - 1) * sizeof(PSAPI_WORKING_SET_BLOCK));
            mMemBuffer = new BYTE[mCb];
            mpPv = reinterpret_cast<PSAPI_WORKING_SET_INFORMATION*>(mMemBuffer);

            if (QueryWorkingSet(GetCurrentProcess(), mpPv, mCb))
                getEntries();
        }
    }

    PROCESS_MEMORY_COUNTERS_EX pmc;
    if (GetProcessMemoryInfo(GetCurrentProcess(), reinterpret_cast<PROCESS_MEMORY_COUNTERS*>(&pmc), sizeof(pmc)))
    {
        mPhysMemUsage = static_cast<double>(pmc.WorkingSetSize);
        mVMemUsage = static_cast<double>(pmc.PrivateUsage);
        //pmc.PageFaultCount
        //pmc.PeakWorkingSetSize
        //pmc.WorkingSetSize
        //pmc.QuotaPeakPagedPoolUsage
        //pmc.QuotaPagedPoolUsage
        //pmc.QuotaPeakNonPagedPoolUsage
        //pmc.QuotaNonPagedPoolUsage
        //pmc.PagefileUsage
        //pmc.PeakPagefileUsage
    }
}

///@function FreePV()
///@desc Free the meminfo data memories
void MemInfo::FreePV() {
    if (mMemBuffer != nullptr)
        delete[] mMemBuffer;

    mMemBuffer = nullptr;
    mpPv = nullptr;
}
